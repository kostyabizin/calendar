<?php

include_once 'C:/MAMP/htdocs/sys/core/init.inc.php';
include_once 'C:/MAMP/htdocs/sys/config/db-cred.inc.php';

spl_autoload_register (function ($class_name){

    $filename = 'C:/MAMP/htdocs/sys/class/class.' .($class_name). '.inc.php';
    if ( file_exists($filename) )
    {
        include_once $filename;
    }

});



$actions = array(
    'event_edit' => array(
    'object' => 'Calendar',
    'method' => 'processForm',
    'header' => 'Location: ../../'
    )
);


if ( $_POST['token']==$_SESSION['token'] && isset($actions[$_POST['action']]) )

{
    $use_array = $actions[$_POST['action']];
    $obj = new $use_array['object']($dbo);

    if ( TRUE === $msg = $obj->$use_array['method']() )

    {
        header($use_array['header']);
        exit;
    }
    else
    {
        die ( $msg );
    }
}
else
{
    header("Location: ../../");
    exit;
}

?>